var $ = jQuery.noConflict();

$(document).ready(function() {

    layoutCloseBtn();
    
    var tux = $('.main-content .tux');

    TweenMax.to(tux, 1, {
        scale: 1,
        ease: Elastic.easeOut.config(1, .3)
    });

    TweenMax.staggerTo($('.distro'), 1, {
        scale: 1,
        delay: 1,
        ease: Elastic.easeOut.config(1, .3)
    }, .5);
    
    tux.on('click', handleTuxClick);
    $('.distro').on('click', handleDistroClick);    
});

var layoutCloseBtn = function() {
    var btn = $('.close-btn');
    btn.on('click', handleCloseClick);

    if (window.innerWidth < 576) {
        btn.offset({ left: (window.innerWidth - btn.outerWidth()) / 2 });
    }
}

var handleTuxClick = function() {
    var tl = new TimelineMax();
    var tux = $('.main-content .tux');
    var popup = $('.popup.linux');
    popup.addClass('active-item');

    tl.to(tux, .1, {
        rotation: 15
    }).to(tux, .1, {
        rotation: -15
    }).to(tux, .1, {
        rotation: 0
    }).to(popup, .5, {
        scale: 1,
        onComplete: animateCloseButton
    });
}

var handleDistroClick = function(e) {
    var target = $(e.target).parent().data('distro');
    var elem = $('.popup.' + target);
    elem.addClass('active-item');
      
    var tl = new TimelineMax();
    var isRotated = elem.data('isRotated') ? true : false;

    tl.to($(e.target), .5, {
        rotationY: isRotated ? 0 : 360,
        onComplete: function() {
            elem.data('isRotated', !isRotated);
        }
    })
    .to(elem, .5, {
        scale: 1,
        onComplete: animateCloseButton
    });    
}

var animateCloseButton = function() {
    var btn = $('.close-btn');
    btn.addClass('active-item');

    TweenMax.to(btn, .2, {
        scale: 1
    });
}

var handleCloseClick = function() {
    var tl = new TimelineMax();

    tl.to($('.active-item'), .2, {
        scale: 0,
        onComplete: function() {
            $('.active-item').removeClass('active-item');
        }
    });
}