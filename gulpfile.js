var gulp = require('gulp');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var connect = require('connect');
var serveStatic = require('serve-static');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var pump = require('pump');
var concat = require('gulp-concat');

var paths = {
    dev: './src/',
    devDeps: './src/deps/',
    prod: './dist/'
}

gulp.task('watch', ['styles', 'scripts', 'serve'], function() {
    gulp.watch(paths.dev + 'sass/**/*.scss', ['styles']);
    gulp.watch(paths.dev + 'js/**/**.js', ['scripts']);
});

gulp.task('styles', function() {
    gulp.src(paths.dev + 'sass/**/**.scss')
        .pipe(sass().on('error', function(error) {
            return notify().write(error)
        }))
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.prod + 'css/'));
});

gulp.task('scripts', function(cb) {
    pump([
        gulp.src([paths.devDeps + '/js/**/**.js', paths.dev + '/js/**/**.js']),
        concat('distros.min.js'),
        uglify(),
        gulp.dest(paths.prod + 'js/')
    ], cb);
});

gulp.task('serve', function() {
    var port = 3000;
    connect().use(serveStatic(__dirname)).listen(port, function() {
        gutil.log('Serving on: http://localhost:' + port);
    });
});

gulp.task('copy-assets', function() {
    gulp.src('./node_modules/normalize.css/normalize.css')
        .pipe(rename('_normalize.scss'))
        .pipe(gulp.dest(paths.dev + 'sass/'));

    gulp.src('./node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest(paths.devDeps + 'js/'));

    gulp.src('./node_modules/gsap/src/minified/TweenMax.min.js')
        .pipe(gulp.dest(paths.devDeps + 'js/'));
});